"use client";

import { useState } from "react";
import {
  createBrowserRouter,
  RouterProvider
} from "react-router-dom";

import Axios from "axios";
import ErrorLayout from "../component/layout/ErrorLayout";
import Footer from "../component/layout/Footer";
import Header from "../component/layout/Header";
import RootLayout from "../component/layout/RootLayout";
import { useAppSelector } from "../lib/hook";
import { assignPrincipal } from "../lib/reducer/AuthSlice";
import { AuthState } from "../lib/type/AuthState";
import { Principal, UserInfoData } from "../login/type";
import { useSearchParams } from "next/navigation";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorLayout />,
    children: [
      {
        index: true,
        path: "dashboard",
        element: <h1>Homepage</h1>
      },
      {
        path: "products",
        element: <h1>List of Products</h1>
      },
      {
        path: "createProduct",
        element: <h1>Create Product</h1>
      },
      {
        path: "settings",
        element: <p>Settings</p>
      }
    ]
  }
]);

type AuthData = {
  idState: string | null,
  authCode: string | null,
  idToken: string | null,
  token: string | null
}

const DashboardPage = () => {
  const principal = useAppSelector<Principal>(
    (state: { auth: AuthState }) => state.auth.principal
  );
  const searchParams = useSearchParams();
  console.log(searchParams.get("code"));
  const [loggedUser, setLoggedUser] = useState<UserInfoData>();

  if (!principal.isAuthenticated) {
    const doSomenting = new Promise<AuthData>(resolve => {
      const authData: AuthData = {
        idState: searchParams.get("state"),
        authCode: searchParams.get("code"),
        idToken: searchParams.get("id_token"),
        token: searchParams.get("access_token")
      }
      console.log("check params: \n" + JSON.stringify(authData));
      resolve(authData);
    }).then(authData => {
      const clientId = process.env.KEYCLOAK_CLIENT_ID;
      const clientSecret = process.env.KEYCLOAK_CLIENT_SECRET;
      const authUrl = process.env.KEYCLOAK_ISSUER;
      const data = {
        grant_type: "authorization_code",
        code: (authData.authCode),
        redirect_uri: encodeURI("http://localhost:9000/dashboard")
      }
      console.log("print req: " + JSON.stringify(data));
      Axios.post(authUrl, data, {
        headers: {
          "Authorization": ("Basic " + btoa(clientId + ":" + clientSecret)),
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }).then(async resp => {
        principal.token = resp.data.access_token;
        principal.refreshToken = resp.data.refresh_token;
        principal.isAuthenticated = true;

        const tokenUrl = "http://localhost:9091/realms/{realm-name}/protocol/openid-connect/userinfo";
        const userInfo = await Axios.get(tokenUrl, { headers: { "Authorization": "Bearer " + resp.data.access_token } });

        principal.username = userInfo.data.preferred_username;
        const userData: UserInfoData = {
          username: userInfo.data.preferred_username,
          name: userInfo.data.name,
          address: userInfo.data.address,
          email: userInfo.data.email,
        }
        setLoggedUser(userData);

        assignPrincipal({ ...principal });
      }).catch(err => {
        console.log(err);
        console.log('Authentication failed!');
      });
    });
  }

  return (
    <div className="h-screen flex flex-col">
      <Header
        employeeName={loggedUser?.name != undefined ? loggedUser.name : "no-name"}
        employeeTitle="Super Admin"
        profilePhoto="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
      ></Header>
      <RouterProvider router={router}></RouterProvider>
      <Footer></Footer>
    </div>
  );
};

export default DashboardPage;
