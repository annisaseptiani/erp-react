"use client";

import React, { useEffect } from 'react';
import { deleteFromLocalStorage, parseJwt, saveToLocalStorage } from '../utils';
import Loading from '../component/layout/Loading';

const CallbackComponent: React.FC = () => {


type AuthData = {
    idState: string | null,
    authCode: string | null,
    idToken: string | null,
    token: string | null
  }


useEffect(() => {
const searchParams = new URLSearchParams(location.hash.substring(1));
const logout = searchParams.get("logout")
    if (logout === "true") {
      deleteFromLocalStorage("profile");
      deleteFromLocalStorage("authData");
      window.location.href = "/login";
    } else {
    const authData: AuthData = {
        idState: searchParams.get("state"),
        authCode: searchParams.get("code"),
        idToken: searchParams.get("id_token"),
        token: searchParams.get("access_token")
      }

    if (authData.idToken) {
        const parsedToken = parseJwt(authData.idToken);
        saveToLocalStorage('profile', parsedToken);
        saveToLocalStorage('authData', authData);
        window.location.href = "/dashboard";
    }
  }
});
return (
<div>
<Loading/>
</div>
);
};

export default CallbackComponent;
