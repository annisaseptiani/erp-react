import "./globals.css";
import { Metadata } from "next";
import * as React from "react";
import AppProvider from "@/app/lib/AppProvider";

export const metadata: Metadata = {
  title: "ERP",
  description: "ERP",
};

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>): React.ReactNode {
  return (
    <AppProvider>
      <html lang="en">
        <body>{children}</body>
      </html>
    </AppProvider>
  );
}
