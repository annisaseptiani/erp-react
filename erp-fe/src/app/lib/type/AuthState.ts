import {Principal} from "@/app/login/type";

export interface AuthState {
    principal: Principal
}
