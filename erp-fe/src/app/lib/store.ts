import {Action} from "redux"
import authReducer from "@/app/lib/reducer/AuthSlice"
import {configureStore} from "@reduxjs/toolkit"
import {ThunkAction} from "redux-thunk"


export const makeStore = () => {
    return configureStore({
        reducer: {
            auth: authReducer
        }
    })
}

export type AppStore = ReturnType<typeof makeStore>
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
export type AppThunk = ThunkAction<void, RootState, unknown, Action<any>>
