"use client"

import {Provider} from "react-redux"
import {AppStore, makeStore} from "@/app/lib/store";
import React, {useRef} from "react";

const AppProvider = ({children}: {children : React.ReactNode}) => {
    const storeRef = useRef<AppStore| null>(null)

    storeRef.current || (storeRef.current = makeStore())

    return (<Provider store={storeRef.current}>{children}</Provider>)
}

export default AppProvider