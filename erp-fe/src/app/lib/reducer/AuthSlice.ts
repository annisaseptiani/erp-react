import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {Principal} from "@/app/login/type";
import {AuthState} from "@/app/lib/type/AuthState";

const initialState: AuthState = {
    principal: {
        username: '',
        token: null,
        isAuthenticated: false
    }
};

const authReducer = createSlice({
    name: "auth",
    initialState,
    reducers: {
        assignPrincipal(state, action: PayloadAction<Principal>) {
            state.principal = {...action.payload}
        },
    },
});
export const { assignPrincipal } = authReducer.actions;

export default authReducer.reducer;
