import { jwtDecode } from "jwt-decode";

export const parseJwt = (token: string) => {
    return jwtDecode(token);
};

export const saveToLocalStorage = (key: string, value: any) => {
    localStorage.setItem(key, JSON.stringify(value));
};

export const getFromLocalStorage = (key: string) => {
    return JSON.parse(localStorage.getItem(key) as string);
};

export const deleteFromLocalStorage = (key: string) => {
    localStorage.removeItem(key);
};
