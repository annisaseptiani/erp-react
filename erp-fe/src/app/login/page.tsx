"use client";

import React from "react";
import LoginForm from "@/app/component/LoginForm";
import "./login.css"

const LoginPage = () => {
  return <LoginForm />;
};

export default LoginPage;
