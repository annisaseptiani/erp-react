export type LoginFormRequest = {
    username: string
    password: string
}

export type LoginWebRequest = {
    username: string
    password: string
}

export type Principal = {
    username: string,
    token: string | null,
    refreshToken?: string,
    isAuthenticated: boolean,
    idToken?: string,
    idState?: string
}

export type UserInfoData = {
    username: string,
    name: string,
    email?: string,
    address?: string,
    contactInfo?: string,
}

