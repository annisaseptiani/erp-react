import Keycloak, { KeycloakInitOptions } from "keycloak-js";

class KeycloakInstance {

  private value: Keycloak;

  constructor() {

    this.value = new Keycloak({
      url: "http://localhost:9091/",
      realm: "realm-name",
      clientId: "login-app"
    });
    this.get().init({
      redirectUri: 'http://localhost:9000',
      checkLoginIframe: false,
      onLoad: "check-sso"
    }).then(authStatus => {
      console.log('init success.');
      console.log(this.value)
    }).catch(err => {
      console.error(err);
    });
  }

  get(): Keycloak {
    return this.value;
  }

}

const kcInstance = new KeycloakInstance;

export default kcInstance;