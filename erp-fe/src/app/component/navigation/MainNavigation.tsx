import Menu from "./Menu";
import { MenuItem } from "./Menu";
import classes from "./MainNavigation.module.css";

const MainNavigation = () => {
  let productMenuItems: MenuItem[] = [
    {
      title: "Create Product",
      link: "/createProduct",
    },
    {
      title: "View Product",
      link: "/products",
    },
  ];
  let settingsMenuItems: MenuItem[] = [
    {
        title: "View Settings",
        link: "/settings"
    }
  ];

  return (
    <>
      <nav className="bg-gray-800 w-64 h-100">
        <ul>
          <li>
            <Menu title="Product" menuItems={productMenuItems}></Menu>
          </li>
          <li>
            <Menu title="Settings" menuItems={settingsMenuItems}></Menu>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default MainNavigation;
