"use client";

import React from "react";
import { NavLink } from "react-router-dom";

interface MenuItem {
  title: string;
  link: string;
}

interface MenuProps {
  title: string;
  menuItems: MenuItem[];
}

const Menu: React.FC<MenuProps> = ({ title, menuItems }) => {
  return (
    <>
      <div className="py-4 px-6 flex items-center justify-center border-b border-gray-700">
        <h1 className="text-white text-lg font-semibold">{title}</h1>
      </div>
      <ul>
        {menuItems.map((menuItem, index) => (
          <li key={index} className="text-white hover:bg-gray-700 px-4 py-2">
            <NavLink to={menuItem.link}>{menuItem.title}</NavLink>
          </li>
        ))}
      </ul>
    </>
  );
};

export type { MenuItem };

export default Menu;
