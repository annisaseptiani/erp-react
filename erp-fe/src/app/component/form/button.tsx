"use client"
import React from 'react';

interface ButtonProps {
  label: string;
  onClick?: () => void;
  className?: string;
}

const Button: React.FC<ButtonProps> = ({ label, onClick, className }) => {
  const buttonClassName = className ? className : "btn rounded-full btn-primary";
  return (
    <button
      className={buttonClassName}
      onClick={onClick}
    >
      {label}
    </button>
  );
};


export default Button;