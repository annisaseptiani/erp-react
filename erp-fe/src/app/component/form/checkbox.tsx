"use client";
import React from 'react';

interface CheckBoxProps {
    label: string;
    checked: boolean;
    onChange: (checked: boolean) => void;
}

const CheckBox: React.FC<CheckBoxProps> = ({label, checked, onChange}) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(e.target.checked);
      };
    return (
        <div className="form-control w-1/2">
            <input 
            type="checkbox" 
            className="checkbox border-2 rounded-sm checkbox-xs checkbox-primary"
            onChange={handleChange}
            checked={checked}/>
            <span className="label-text">{label}</span>
        </div>
    );
};


export default CheckBox;


