"use client";
import Button from '@/app/component/form/button';
import Checkbox from '@/app/component/form/checkbox';
import FormInput from '@/app/component/form/forminput';
import React, {useState} from 'react';


const LoginForm: React.FC = () => {   
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isChecked, setRememberMe] = useState(false);
  
    const handleUsernameChange = (value: string) => {
      setUsername(value);
    };
  
    const handlePasswordChange = (value: string) => {
      setPassword(value);
    };
  
    const handleRememberMeChange = (checked: boolean) => {
      setRememberMe(checked);
    };
  
    const handleSubmit = (e: React.FormEvent) => {
      e.preventDefault();
      console.log('Username:', username);
      console.log('Password:', password);
      console.log('Remember Me:', isChecked);
    };

    return (
        <form className="w-full max-w-lg" onSubmit={handleSubmit}>
            <FormInput label="Username" type="text" placeholder='username here...' onChange={handleUsernameChange}/>
            <FormInput label="Password" type="password" placeholder='password here...' onChange={handlePasswordChange}/>
            <Checkbox label="Remember me" checked={isChecked} onChange={handleRememberMeChange}/>
            <div className="mt-6">
                <Button label="Login"/>
            </div>
        </form>
    );
  };

  export default LoginForm;
