"use client";
import React from 'react';

interface FormProps {
    onChange: (value: string)=> void;
    placeholder?: string;
    label: string;
    value?: string;
    type: string;
}

const FormInput: React.FC<FormProps> = ({ onChange, placeholder, label, value, type }) => {
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(e.target.value);
    };
    return (
        <div className="form-control w-full">
            <label className="label">
                <span className="label-text">{label}<span className="text-error">*</span></span>
            </label>
            <input type={type} placeholder={placeholder} className="input w-full rounded-sm bg-gray-50" value={value} onChange={handleChange} />
        </div>
    );
};


export default FormInput;

