"use client";

import { useAppSelector } from "@/app/lib/hook";
import { AuthState } from "@/app/lib/type/AuthState";
import { Principal } from "@/app/login/type";
import { assignPrincipal } from "@/app/lib/reducer/AuthSlice"
import Image from 'next/image';
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import "../login/login.css";
import { deleteFromLocalStorage } from "../utils";

const LoginForm = () => {
    const router = useRouter();
    const principal = useAppSelector<Principal>(
        (state: { auth: AuthState }) => state.auth.principal
    );

    useEffect(() => {
        
        console.log("check principal " + principal.isAuthenticated)
        console.log("check principal " + principal)

        const searchParams = new URLSearchParams(location.hash.substring(1));
        const logout = searchParams.get("logout")
        if (logout === "true") {
            deleteFromLocalStorage("profile");
            deleteFromLocalStorage("authData");
        } 
    
    }, []);

   
    const handleLoginKeycloak = () => {
        const href: string = 'http://localhost:9091/realms/{client-name}' + '/protocol/openid-connect/auth?'
            + 'response_type=code%20token%20id_token&client_id=' + '{app-name}' + '&'
            + 'redirect_uri=http%3A%2F%2Flocalhost%3A9000%2Fdashboard&'
            + 'scope=openid%20profile&state=somestate&nonce=oncetest';
        router.push(href)
        assignPrincipal({...principal});
    };

    return (
        <div id="authPg" className="bg-login">
            <div style={{ position: 'absolute', transform: 'translate(-50%, -50%)', top: '45%', left: '50%', margin: '0rem', width: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                <div style={{ marginLeft: 'auto', marginRight: 'auto', display: 'block', marginBottom: '1.75rem' }}>
                    <Image alt="logo" src="/comp-logo.svg" width={142} height={38} />
                </div>
                <div className="card-wrap">
                    <div className="white-card">
                        <div style={{ display: 'flex' }}>
                            <div className="title-wrap">
                                <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl text-black">
                                    ERP
                                </h1>
                                <div className="login-image">
                                    
                                </div>
                            </div>
                            <div className="login-card-wrap">
                                <h5 className="MuiTypography-root MuiTypography-h5 font-signin css-zq6grw text-black">Sign in</h5>
                                <button className="login-btn-keycloak" onClick={handleLoginKeycloak}>
                                    <div className="login-logo">
                                        
                                    </div>
                                    <h6 className="MuiTypography-root MuiTypography-h6 css-1anx036 font-btn" >Login With Keycloak</h6>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default LoginForm;