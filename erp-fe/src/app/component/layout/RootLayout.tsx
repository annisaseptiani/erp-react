import { Outlet } from "react-router-dom";
import MainNavigation from "../navigation/MainNavigation";

const RootLayout = () => {
  return (
    <>
      <div className="flex grow">
        <MainNavigation></MainNavigation>
        {/* Marker where childs element should render */}
        <main className="grow">
          <Outlet />
        </main>
      </div>
    </>
  );
};

export default RootLayout;
