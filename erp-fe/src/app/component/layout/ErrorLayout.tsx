import MainNavigation from "../navigation/MainNavigation";

const ErrorLayout = () => {
  return (
    <>
      <MainNavigation></MainNavigation>
      {/* Marker where childs element should render */}
      <main>
        <h1>An error occured</h1>
        <p>Could not find this page</p>
      </main>
    </>
  );
};

export default ErrorLayout;
