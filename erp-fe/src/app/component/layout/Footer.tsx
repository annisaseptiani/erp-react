"use client";

import classes from "./Footer.module.css";

const Footer = () => {
  return (
    <>
      <footer className={"w-full justify-center " + classes.footer}>
            <h1 className={classes.h1}> © ERP</h1>
            
      </footer>
    </>
  );
};

export default Footer;
