"use client";

import React, { useEffect } from "react";
import classes from "./Header.module.css";
import { HiChevronDown } from "react-icons/hi";
import { CiSettings } from "react-icons/ci";
import { IoIosNotificationsOutline } from "react-icons/io";
import { useRouter } from "next/navigation";
import LogoutPanel from "../LogoutPanel";
import { useDispatch } from "react-redux";
import { useAppDispatch, useAppSelector } from "@/app/lib/hook";
import { Principal } from "@/app/login/type";
import { AuthState } from "@/app/lib/type/AuthState";
import kcInstance from "@/app/keycloak/keycloak-cfg";
import { Axios } from "axios";
import { deleteFromLocalStorage } from "@/app/utils";
;

interface HeaderProps {
  employeeName: string;
  employeeTitle: string;
  profilePhoto: string;
}

const Header = ({
  employeeName,
  employeeTitle,
  profilePhoto,
}: HeaderProps) => {
  const router = useRouter();

  const principal = useAppSelector<Principal>(
    (state: { auth: AuthState }) => state.auth.principal
); 
  const logouthandler = async() => {
    const keycloakDomain = process.env.KEYCLOAK_ISSUER;;
    const clientId = process.env.KEYCLOAK_CLIENT_ID;
    const idToken = principal.idToken;
    const redirectUri = 'http://localhost:9000/login#logout=true';

    const logoutUrl = `${keycloakDomain}/realms/{realms-name}/protocol/openid-connect/logout?&client_id=${clientId}&hint_token_id=${idToken}&post_logout_redirect_uri=${encodeURIComponent(redirectUri)}`;
    
    router.push(logoutUrl)
  };

  return (
    <header className="navbar bg-base-100 border-b border-grey flex justify-between">
      <div className="flex-col items-center">
        <div className="ml-2 mr-5">
          <h1 className="text-lg font-bold text-stone-500 align-baseline">Dashboard</h1>
          <h6 className="block text-sm text-stone-500 align-baseline">With all styling tools options available in today&apos;s market</h6>
        </div>
      </div>

      <div className="flex-none ml-2 relative">
      <label className="relative block">
        <span className="sr-only">Search</span>
        <span className="absolute inset-y-0 left-0 flex items-center pl-2">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" className="w-4 h-4 opacity-70"><path fillRule="evenodd" d="M9.965 11.026a5 5 0 1 1 1.06-1.06l2.755 2.754a.75.75 0 1 1-1.06 1.06l-2.755-2.754ZM10.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z" clipRule="evenodd" /></svg>
        </span>
        <input className="placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm" placeholder="Search Something" type="text" name="search"/>
      </label>
        <IoIosNotificationsOutline className="text-2xl text-gray-500 ml-10 cursor mr-5"/>
        <CiSettings className="text-2xl text-gray mr-10 ml-5"/>
        
          <div className="avatar">
            <div className="w-16 rounded-full">
              <img src={profilePhoto} />
            </div>
          </div>
          <div className={"flex-none relative " + classes.profile}>
            <div className="cursor-pointer flex items-center mr-5">
              <div className="ml-2 mr-5">
                <span className="text-base font-medium">{employeeName}</span>
                <span className="block text-xs text-gray-500">{employeeTitle}</span>
              </div>
            </div>
            <div className="flex-none">
              <ul className={"absolute hidden text-gray-700 pt-1 " + classes.dropdown}>
                <li>
                  <button className="rounded-t bg-gray-200 hover:bg-gray-400 py-2 px-12 block whitespace-no-wrap" onClick={logouthandler}>Logout</button>
                </li>
              </ul>
            </div>
            <HiChevronDown className="absolute top-0 right-0 m-2 text-gray-500 mt-3" />
          </div>
      </div>
    </header>
  );
};

export default Header;
