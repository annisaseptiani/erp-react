/** @type {import('next').NextConfig} */
const nextConfig = {
	distDir: process.env.BUILD_DIRECTORY || "dist"
};

export default nextConfig;
